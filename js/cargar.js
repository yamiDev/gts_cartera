/* jshint -W119 */
/* jshint -W104 */
$(document).ready(function(){
	$('.enviar_archivo').on('click',function(evt){
		$("input[name='enviar']").attr("disabled","true");
		evt.preventDefault();
		cargarArchivoCSV();
		$("input[name='enviar']").removeAttr("disabled");
	});
});

function cargarArchivoCSV(){
	
	var archivo = $('input[name="archivo_csv"]').val();
	var extension = $('#archivo_csv').val().split(".").pop().toLowerCase();
	var Formulario = document.getElementById('frmSubirCSV');
	var dataForm = new FormData(Formulario);
	var retornarError = false;
	if (archivo==""){
		$("#archivo_csv").addClass('alert-danger');
		const Toast = Swal.mixin({
		  toast: true,
		  position: 'top-end',
		  showConfirmButton: false,
		  timer: 4000
		});

		Toast.fire({
		  type: 'warning',
		  title: 'Debe subir un Archivo'
		});
	}else if($.inArray(extension, ['csv']) == -1){
		swal.fire({
			title:"Oops!!",
			type:"error",
			html:"El archivo que está intentando subir no es válido"
		});
		retornarError = true;
		$('#archivo_csv').val("");
	}else{
		retornarError = true;
		$("#archivo_csv").focus();
		$("#archivo_csv").removeClass('alert-danger');
	}
  $.ajax({
		url: 'procesar.php',
		type: 'POST',
		data: dataForm,
		cache: false,
		contentType: false,
		processData: false,
    beforeSend: function(){
      $('#estado').addClass('preloader');
    },
    success: function(data){
			$('#estado').removeClass('preloader');
      $('#estado').fadeOut("fast",function(){
        $('#estado').html(data);
      });
          
      $('#estado').fadeIn("slow");
      $("#frmSubirCSV").find('input[type=file]').val("");
    },
		error: function (jqXHR, textStatus, errorThrown) {
	  	$loader.html(jqXHR + " :: " + textStatus + " :: " + errorThrown);
		}
	});
}