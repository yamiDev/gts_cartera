/*jshint -W061*/
/*jshint -W119*/

var listar = function(arg, nombre_tabla){
  var table = $('#list_data').DataTable({
    "destroy":true,
    select:true,
    dom: 'Bfrtip',
		"ajax":{
			"method":"POST",
			"url":"listarData/listar.php",
      "data":{"programa":arg,"tabla":nombre_tabla},
      "cache": false,
		},
		"columns":[
			{"data":"referencia_catastral"},
			{"data":"nit_cedula"},
			{"data":"nombre"},
			{"data":"direccion"},
			{"data":"vigencia"},
			{"data":"avaluo"},
			{"data":"predial"},
			{"data":"valor"},
      {"defaultContent":
        "<button type='button' class='mover btn btn-warning btn-sm btn-tooltip' rel='tooltip' data-toggle='modal' data-target='#modalMover' title='Mover'><i class='fa fa-copy'></i></button>"
      }
		],
		"language":idioma_espanol,
		buttons: [
      {
        extend: 'excelHtml5',
        title: 'Gestion Cartera'+' '+$("#programa option:selected" ).text(),
        sheetName: $("#programa option:selected" ).text()
      },
      {
        extend: 'pdfHtml5',
        title: 'Gestion Cartera'+' '+ $("#programa option:selected" ).text()
      },
      'csv', 'print',
    ],
	});

  obtener_data_mover("#list_data tbody", table, arg, nombre_tabla);
};


var obtener_data_mover = function(tbody, table, arg, nombre_tabla) {
  $(document).on('click', '.mover',function() {
    var data = table.row($(this).parents("tr")).data();
    $('#id_reg').val(data.id);
    console.log(data);
    $('#modalMover').on('click', 'button.guardar',function(){
      var tabla = nombre_tabla;
      var id = $('#id_reg').val();
      var prog = $('#moverA').val();
      console.log(id, prog, tabla);
      $.ajax({
        url: 'listarData/listar.php',
        type: 'POST',
        dataType: 'json',
        data: { "id":id, "prog": prog, "tabla":tabla},
        success: function(msg){
          if (msg == "1"){
            Swal.fire({
              type: 'success',
              title: 'Movido con éxito',
              showConfirmButton: false,
              timer: 2500
            }).then(function(isConfirm){
              if(isConfirm){
                location.reload();
              }
            });
            /* 
              .then(function(isConfirm){
                if(isConfirm){
                  location.reload();
                }
              }) 
            */
          }
        }
      });
    });
  });
}; 

function listarTablas(){
  var selector = "tablas";
  $.ajax({
    url: './listarData/listar.php',
    type: 'post',
    dataType: 'json',
    data:{"tablas":selector},
    success: function(msg){
      var valores = eval(msg);
      var cadena;
      var opcion;
      for (var i=0; i< valores.length; i++) {
        cadena = valores[i][0];
        opcion = cadena.replace('_'," ");
        $("#tablas").append('<option  value='+valores[i][0]+'>'+opcion.toUpperCase()+'</option>');		
      }
    }
  });	
}


var idioma_espanol={
  "sProcessing":     "Procesando...",
  "sLengthMenu":     "Mostrar _MENU_ registros",
  "sZeroRecords":    "No se encontraron resultados",
  "sEmptyTable":     "Sin datos disponibles aún",
  "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
  "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
  "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
  "sInfoPostFix":    "",
  "sSearch":         "Buscar:",
  "sUrl":            "",
  "sInfoThousands":  ",",
  "sLoadingRecords": "Cargando...",
  "oPaginate": {
      "sFirst":    "Primero",
      "sLast":     "Último",
      "sNext":     "Sig",
      "sPrevious": "Ant"
  },
  "oAria": {
      "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
      "sSortDescending": ": Activar para ordenar la columna de manera descendente"
  }
};