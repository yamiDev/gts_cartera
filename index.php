<!DOCTYPE html>
<html>
<head>
  <title>Gestión Cartera GTS</title>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" name="viewport" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons"/>
  
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" >
  <link href="./material-dashboard.css?v=2.1.0" rel="stylesheet" />
  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/jq-3.3.1/dt-1.10.18/b-1.5.4/fc-3.2.5/r-2.2.2/sc-1.5.0/datatables.min.css"/>
  <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.4/css/buttons.dataTables.min.css" type="text/css">
  <link rel="stylesheet" href="./animate.css" type="text/css">
  <link rel="stylesheet" href="style.css">
  <link rel="manifest" href="./manifest.json">
  <script src="./core/jquery.min.js"></script>
</head>
<body class="bg-dark">
  <div class="wrapper">
    <div class="sidebar" data-color="purple" data-background-color="black" data-image="./img/sidebar-2.jpg">
      <div class="logo">
        <a href="index.php" class="simple-text logo-normal">
          Gestión Cartera
        </a>
      </div>
      <div class="sidebar-wrapper">
        <ul class="nav">
          <li class="nav-item active btn-izquierda" id="home">
            <a class="nav-link" href="index.php">
              <i class="material-icons">home</i>
              <p>Inicio</p>
            </a>
          </li>
          <li class="nav-item btn-izquierda" id="cargarData" data-contenido="cargar">
            <a class="nav-link" href="#">
              <i class="material-icons">attach_file</i>
              <p>Cargar Archivos</p>
            </a>
          </li>
          <li class="nav-item btn-izquierda" id="listarData" data-contenido="listar">
            <a class="nav-link" href="#">
              <i class="material-icons">attach_money</i>
              <p>Listar Programas</p>
            </a>
          </li>
        </ul>
      </div>
    </div>
    <div class="main-panel">
      <nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top " id="navigation-example">
        <div class="container-fluid">
          <div class="navbar-wrapper">
            <a class="navbar-brand" href="#"></a>
          </div>
          <button class="navbar-toggler" type="button" data-toggle="collapse" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation" data-target="#navigation-example">
            <span class="sr-only">Toggle navigation</span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
          </button>
      </nav>
      <div class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="contenido" id="home">
              <?php require_once ('inicio.php');?>
            </div>
            <div id="cargar" class="col-md-8 offset-md-2 contenido" style="display: none;">
              <?php require_once('cargarData/cargarData.php');?>
            </div>
            <div id="listar" class="col-md-12 contenido" style="display: none;">
              <?php require_once('listarData/listarData.php');?>
            </div>
            <div class="contenido" id="not_found" style="display:none;">
              <svg xmlns="http://www.w3.org/2000/svg" width="250" height="250" viewBox="0 0 250 250">
                <style>.a{opacity:0.05;}.b{fill:none;}.c{fill:#0096D6;}</style>
                <g class="a"><rect x="20.8" y="20.8" width="208.3" height="208.3" class="b"></rect></g><g class="a"><rect width="250" height="250" class="b"></rect></g><path d="M223.1 179.2V56.8c0-6.2-5.1-11.3-11.3-11.3H26.9v122.4c0 6.2 5.1 11.3 11.3 11.3h69.5v20.1H79.5v5.2h92v-5.2h-29.1v-20.1H223.1zM32.1 167.9V50.7h179.7c3.4 0 6.1 2.7 6.1 6.1v117.2H38.2C34.8 174 32.1 171.3 32.1 167.9zM137.1 199.3h-24.2v-20.1h24.2V199.3z" class="c"></path><line x1="102.5" y1="125" x2="147.5" y2="125" style="fill:none;stroke-width:4;stroke:#0096D6"></line><path d="M167.9 97.3c-2.4 1.5-4.8 2.6-7.2 3.3 -2.4 0.7-5 1.1-7.9 1.1 -2.9 0-5.7-0.4-8.2-1.1 -2.5-0.8-5.1-2-7.7-3.8v-2c0-0.2 0.1-0.4 0.2-0.5 0.1-0.2 0.3-0.2 0.5-0.2 0.2 0 0.4 0.1 0.6 0.2 1.3 0.6 2.4 1.2 3.5 1.6 1.1 0.5 2.2 0.9 3.4 1.2 1.1 0.3 2.3 0.5 3.6 0.7 1.2 0.1 2.6 0.2 4 0.2 1.5 0 2.8-0.1 4.1-0.2 1.3-0.1 2.6-0.4 3.8-0.8 1.3-0.3 2.6-0.8 3.9-1.3 1.3-0.5 2.7-1.2 4.2-2v2.4c0 0.4-0.1 0.7-0.2 0.8C168.4 96.9 168.2 97.1 167.9 97.3z" class="c"></path><path d="M112.2 97.3c-2.4 1.5-4.8 2.6-7.2 3.3 -2.4 0.7-5 1.1-7.9 1.1 -2.9 0-5.7-0.4-8.2-1.1 -2.5-0.8-5.1-2-7.7-3.8v-2c0-0.2 0.1-0.4 0.2-0.5 0.1-0.2 0.3-0.2 0.5-0.2 0.2 0 0.4 0.1 0.6 0.2 1.3 0.6 2.4 1.2 3.5 1.6 1.1 0.5 2.2 0.9 3.4 1.2 1.1 0.3 2.3 0.5 3.6 0.7 1.2 0.1 2.6 0.2 4 0.2 1.5 0 2.8-0.1 4.1-0.2 1.3-0.1 2.6-0.4 3.8-0.8 1.3-0.3 2.6-0.8 3.9-1.3 1.3-0.5 2.7-1.2 4.2-2v2.4c0 0.4-0.1 0.7-0.2 0.8C112.8 96.9 112.5 97.1 112.2 97.3z" class="c"></path>
              </svg>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
  <script src="https://cdn.datatables.net/buttons/1.5.4/js/dataTables.buttons.min.js"></script>
  <script src="https://cdn.datatables.net/buttons/1.5.4/js/buttons.flash.min.js"></script>
  <script type="text/javascript" src="https://cdn.datatables.net/v/bs4/jq-3.3.1/dt-1.10.18/b-1.5.4/fc-3.2.5/r-2.2.2/sc-1.5.0/datatables.min.js"></script>
  <script src="./core/popper.min.js"></script>
  <script src="./core/bootstrap-material-design.min.js"></script>
  <script src="https://unpkg.com/default-passive-events"></script>
  <script src="./plugins/perfect-scrollbar.jquery.min.js"></script>
  <script async defer src="https://buttons.github.io/buttons.js"></script>
  <script src="./plugins/chartist.min.js"></script>
  <script src="./plugins/bootstrap-notify.js"></script>
  <script src="./material-dashboard.js?v=2.1.0"></script>
  <script src="https://cdn.jsdelivr.net/npm/sweetalert2"></script>
  <script src="js/jszip.min.js"></script>
  <script src="js/pdfmake.min.js"></script>
  <script src="js/vfs_fonts.js"></script>
  <script src="js/buttons.html5.min.js"></script>
  <script src="js/buttons.print.min.js"></script>
  <script src="js/modernizr-custom.js"></script>
  <script src="js/cargar.js"></script>
  <script src="js/listar.js"></script>
  <script>
		$(document).ready(function(){
      $('.btn-tooltip').tooltip();
      $.fn.dataTable.ext.errMode = 'none';
      $('[data-toggle="tooltip"]').tooltip();
      $('.sidebar .sidebar-wrapper, .main-panel').perfectScrollbar();
      $('select[name="list_data_length"]').css({
        "color":"rgb(183, 170, 170)"
      });
      $('.buttons-html5').removeClass('btn-secondary');
      $('.buttons-html5').addClass('btn-primary');
			$('.btn-izquierda').click(function(){
				var id = $(this).attr('id');
				$('li').removeClass('active');
				$('#'+id).addClass('active');
				$('#'+id).css('font-weight','bold');
			});
			$('.btn-izquierda').click(function(){
				var id = $(this).data('contenido');
        $('.contenido').css('display','none');
        $('#'+id).css('display','block');
			});
		});
    window.addEventListener("load",function() {
      setTimeout(function(){
        window.scrollTo(0, 1);
      }, 0);
    });
	</script>
</body>
</html>