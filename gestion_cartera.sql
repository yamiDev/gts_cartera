-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 15-03-2019 a las 20:14:56
-- Versión del servidor: 10.1.37-MariaDB
-- Versión de PHP: 7.3.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `gestion_cartera`
--

DELIMITER $$
--
-- Procedimientos
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `ASOCIADOS` ()  SELECT nombre, COUNT(nombre) AS total FROM datos WHERE	nombre not LIKE '%-SAS'
      AND nombre not LIKE '%LTDA%' AND nombre not LIKE '%S-A-S%' AND nombre not LIKE '%SOCIEDAD%'
      AND nombre not LIKE '%s.a%'  AND nombre not LIKE '%asociacion%'
      AND nombre not LIKE '%compañia%' AND nombre not LIKE '%inversiones%'
      AND NOMBRE not LIKE '%e hijos%'  AND nombre not LIKE '%CIA S EN C%'
    	AND nombre not LIKE '%INMOBILIARIA%' AND nombre not LIKE '%URBANIZACION%'
      AND nombre not LIKE '%URBANIZACION%' AND nombre not LIKE '%IMSTITUTO%'
      AND nombre not LIKE '%INSTITUTO%' AND nombre not LIKE '%CORPORACION%'
      AND nombre not LIKE '%E-S-E%' AND nombre not LIKE '%AEROCIVIL%'
      AND nombre not LIKE '%ACUEDUCTO%' AND nombre not LIKE '%AEROPUERTO%'
      AND nombre not LIKE '%FUNDACION%' AND nombre not LIKE '%PLAN%'
      AND nombre not LIKE '%-S-A-S%' AND nombre not LIKE '%COORPORACION%'
      AND nombre not LIKE '%CORSOCIAL%' AND nombre not LIKE '%AGENCIA%'
      AND ( direccion like '%C %' or direccion LIKE '%K %' or direccion LIKE '%LO %' or direccion LIKE '%Lote %' ) 
GROUP BY nombre having COUNT(nombre) > 1
ORDER BY nombre ASC$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `datos`
--

CREATE TABLE `datos` (
  `referencia_catastral` varchar(500) COLLATE utf8_spanish_ci NOT NULL,
  `nit_cedula` varchar(500) COLLATE utf8_spanish_ci NOT NULL,
  `nombre` varchar(500) COLLATE utf8_spanish_ci NOT NULL,
  `direccion` varchar(500) COLLATE utf8_spanish_ci NOT NULL,
  `vigencia` varchar(500) COLLATE utf8_spanish_ci NOT NULL,
  `avaluo` varchar(500) COLLATE utf8_spanish_ci NOT NULL,
  `predial` varchar(500) COLLATE utf8_spanish_ci NOT NULL,
  `valor` varchar(500) COLLATE utf8_spanish_ci NOT NULL,
  `tipo` int(11) NOT NULL DEFAULT '0',
  `data_upload` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tokens_consulta`
--

CREATE TABLE `tokens_consulta` (
  `id_token` int(11) NOT NULL,
  `tokens` varchar(200) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `datos`
--
ALTER TABLE `datos`
  ADD KEY `nombre` (`nombre`(255));

--
-- Indices de la tabla `tokens_consulta`
--
ALTER TABLE `tokens_consulta`
  ADD PRIMARY KEY (`id_token`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `tokens_consulta`
--
ALTER TABLE `tokens_consulta`
  MODIFY `id_token` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
