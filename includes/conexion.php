<?php 
	
	define("SERVER", "localhost");
	define("USER","root");
	define("PASS","");
	define("BD","gestion_cartera");

	function Conexion(){
		if (!($cn=mysqli_connect(SERVER,USER,PASS,BD))){
			echo "Error conectando a la base de datos.";
			exit();
		}
		if(!mysqli_select_db($cn,BD)){
			echo "Hay un error en la conexión a la base de datos.";
			exit();
		}
		return $cn;
  }
