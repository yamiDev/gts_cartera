<div class="card">
	<div class="card-header card-header-primary">
		<h4 class="card-title text-center" style="font-weight: bold;">Subir Cartera</h4>
		<p class="card-category">Carga de archivos CSV</p>
	</div>
	<div class="card-body">
		<form method="post" id="frmSubirCSV" enctype="multipart/form-data">
      <div class="row">
      	<div class="col-md-6 text-center">
      		<div>
						<span class="btn btn-raised btn-round btn-default btn-file">
							<input type="file" name="archivo_csv" id="archivo_csv">
						</span>
					</div>
      	</div>
			</div>
      <input type="submit" name="enviar" class="enviar_archivo btn btn-primary pull-right" value="Subir Archivo">
      <div id="estado"></div>
    </form>
	</div>
</div>