<div class="card">
	<input type="hidden" id="nombre_archivo">
	<div class="card-header card-header-primary">
		<h4 class="card-title text-center" style="font-weight: bold;">Listar Programas</h4>
	</div>
	<div class="card-body">
		<div class="row">
			<div class="form-group col-md-6">
				<label for="tablas" style="margin-left: 3%;margin-top: -1%;"><b>Seleccione cartera:</b></label>
				<select name="tablas" class="custom-select" id="tablas" style="color:#3d3d3d;">
					<option>Seleccione una Cartera</option>
				</select>
			</div>
			<div class="col-md-6 form-group">
				<label for="programa" style="margin-left: 3%;margin-top: -1%;"><b>Seleccione Programa:</b></label>
				<select name="programa" class="custom-select" name="programa" id="programa" style="color:#3d3d3d;">
					<option class="selected">seleccione una opción</option>
					<option value="1">Juridicos</option>
					<option value="5">Asociados</option>
					<option value="6">Urbanos</option>
					<option value="3">Institucionales</option>
					<option value="4">Municipio</option>
					<option value="2">Religiosos</option>
					<option value="0">Restantes</option>
				</select>			
			</div>
		</div>
		<div class="table-responsive" style="display: none;" id="table_data">
			<table id="list_data" class="table table-bordered table-hover" data-page-length='25'>
				<thead class="text-primary">
					<tr>
						<th>Ref Catastral</th>
						<th>NIT/Cedula</th>
						<th>Nombre</th>
						<th>Direccion</th>
						<th>Vigencia</th>
						<th>Avalúo</th>
						<th>Predial</th>
						<th>Valor</th>
						<th></th>
					</tr>
				</thead>
			</table>
		</div>
	</div>
</div>
<div class="modal fade" id="modalMover" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Mover al Programa</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
				<input type="hidden" name="id_reg" id="id_reg" value="">
        <select name="moverA" id="moverA" class="custom-select">
        	<option>Seleccione un Programa</option>
        	<option value="1">JURIDICOS</option>
        	<option value="6">URBANOS</option>
        	<option value="5">ASOCIADOS</option>
        	<option value="4">MUNICIPIO</option>
        	<option value="2">RELIGIOSOS</option>
        	<option value="3">INSTITUCIONALES</option>
        </select>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">X</button>
        <button type="button" class="guardar btn btn-success"><i class="material-icons">save</i>Guardar</button>
      </div>
    </div>
  </div>
</div>
<script>
	$(document).ready(function(){
		listarTablas();
		$('#tablas').on('change',function(){	
			var consulta = $('#tablas').val();
			var tabla = $('#table_data');
			$('#programa').on('change',function(){
				var programa = $('#programa').val();
				switch (programa) {
					case "1":
						listar(programa,consulta);
						tabla.css('display','block');
						break;
					case "2":
						listar(programa,consulta);
						tabla.css('display','block');
						break;
					case "3":
						listar(programa,consulta);
						tabla.css('display','block');
						break;
					case "4":
						listar(programa,consulta);
						tabla.css('display','block');
						break;
					case "5":
						listar(programa,consulta);
						tabla.css('display','block');
						break;
					case "6":
						listar(programa,consulta);
						tabla.css('display','block');
						break;
					case "0":
						listar(programa,consulta);
						tabla.css('display','block');
						break;
					default:
						tabla.css('display','none');
						break;
				}
			});
		});
	});
</script>