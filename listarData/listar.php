<?php 

	require_once('../includes/conexion.php');
	
	if(isset($_POST['tablas'])){
		listarTablas();
	}

	if (isset($_POST['programa']) && isset($_POST['tabla'])){
		$programa = $_POST['programa']; $consulta = $_POST['tabla'];
		if($consulta !== 'Seleccione una Cartera'){
			switch ($programa) {
				case "1":
					listarJuridicos($consulta);
					break;
				case "5":
					listarAsociados($consulta);
					break;
				case "6":
					listarUrbanos($consulta);
					break;
				case "3":
					listarInstitucional($consulta);
					break;
				case "4":
					listarMunicipios($consulta);
					break;
				case "2":
					listarReligiosos($consulta);
					break;
				case "0":
					listarRestantes($consulta);
					break;
			}			
		}
	}

	if (isset($_POST['id']) && isset($_POST['prog']) && isset($_POST['tabla'])) {
		$id = $_POST['id']; $prog = $_POST['prog']; $consulta = $_POST['tabla'];

		moverData($consulta, $id, $prog);
	}
	
	function listarTablas(){
		$con = Conexion();

		$sql = "show tables";

		$result = mysqli_query($con,$sql);

		echo json_encode(mysqli_fetch_all($result));
		
		mysqli_free_result($result);
		mysqli_close($con);
	}

	function listarJuridicos($consulta){

		$con = Conexion();

		$sql = "SELECT * FROM $consulta WHERE tipo =1";
		
		$result = mysqli_query($con, $sql);

		if (!$result) {
			echo("Error : " . mysqli_error($con));die();
		}else{
			while ($data = mysqli_fetch_assoc($result)){
				$arreglo["data"][]=$data;
			}
			echo json_encode($arreglo);
		}

		mysqli_free_result($result);
		mysqli_close($con);
	}
	
	function listarAsociados($consulta){
		$con = Conexion();
		
		$sql = "SELECT * FROM $consulta  WHERE tipo = 5";
		$result = mysqli_query($con, $sql);
		if (!$result){
			echo("Error : " . mysqli_error($con));die();
		}else{
			while ($data = mysqli_fetch_assoc($result)) {	
				$arreglo['data'][]=$data;
			}
			echo json_encode($arreglo);
		}
		mysqli_free_result($result);
		mysqli_close($con);
	}

	function listarUrbanos($consulta){
		$con = Conexion();

		$sql = "SELECT * FROM $consulta WHERE tipo = 6";
		
		$result = mysqli_query($con, $sql);

		if (!$result) {
			echo("Error : ".mysqli_error($con));die();
		}else{
			while ($data = mysqli_fetch_assoc($result)){
				$arreglo["data"][]=$data;
			}
			echo json_encode($arreglo);
		}

		mysqli_free_result($result);
		mysqli_close($con);
	}
	
  function listarInstitucional($consulta){
		$con = Conexion();
		
		$sql = "SELECT * FROM $consulta WHERE tipo = 3";
		
		$result = mysqli_query($con, $sql);

		if (!$result) {
			echo("Error : " . mysqli_error($con));die();
		}else{
			while ($data = mysqli_fetch_assoc($result)){
				$arreglo["data"][]=$data;
			}
			echo json_encode($arreglo);
		}

		mysqli_free_result($result);
		mysqli_close($con);
	}
	
	function listarMunicipios($consulta){
		$con = Conexion();

		$sql = "SELECT * FROM $consulta where tipo = 4";
		
		$result = mysqli_query($con, $sql);

		if (!$result) {
			echo("Error : " . mysqli_error($con));die();
		}else{
			while ($data = mysqli_fetch_assoc($result)){
				$arreglo["data"][]=$data;
			}
			echo json_encode($arreglo);
		}

		mysqli_free_result($result);
		mysqli_close($con);
	}

	function listarRestantes($consulta){
		$con = Conexion();

		$sql = "SELECT * FROM $consulta WHERE tipo = 0";
		
		$result = mysqli_query($con, $sql);

		if (!$result) {
			echo("Error : " . mysqli_error($con));die();
		}else{
			while ($data = mysqli_fetch_assoc($result)){
				$arreglo["data"][]=$data;
			}
			echo json_encode($arreglo);
		}

		mysqli_free_result($result);
		mysqli_close($con);
	}

	function listarReligiosos($consulta){
		$con = Conexion();

		$sql = "SELECT * FROM $consulta WHERE tipo = 2";
		
		$result = mysqli_query($con, $sql);

		if (!$result) {
			echo("Error : " . mysqli_error($con));die();
		}else{
			while ($data = mysqli_fetch_assoc($result)){
				$arreglo["data"][]=$data;
			}
			echo json_encode($arreglo);
		}

		mysqli_free_result($result);
		mysqli_close($con);
	}

	function moverData($consulta, $id, $prog) {
		$con = Conexion();

		$upd = "UPDATE $consulta SET tipo = $prog WHERE id = $id";

		$result = mysqli_query($con, $upd);

		if (!$result) {
			echo("Error : " . mysqli_error($con));die();
		}else{
			echo "1";
		}
	}