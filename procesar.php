<?php 
	require_once('includes/conexion.php');
	$con = Conexion();

	if(substr($_FILES['archivo_csv']['name'], -3)=="csv"){
		
		$fecha = date('Y-m-d');
		$carpeta = "C:/xampp/mysql/data/";
		$n_archivo = $fecha.'-'.$_FILES['archivo_csv']['name'];
		$nombre_fichero = substr($_FILES['archivo_csv']['name'],0,-4);

		move_uploaded_file($_FILES['archivo_csv']['tmp_name'], $carpeta.$n_archivo);

		$check_table = "DROP TABLE IF EXISTS datos_".$nombre_fichero."";
		$rpta_checkTable = mysqli_query($con, $check_table)or die(mysqli_error($con));
		$stmt = "CREATE TABLE datos_".$nombre_fichero."(referencia_catastral varchar(500) COLLATE utf8_spanish_ci NOT NULL, nit_cedula varchar(500) COLLATE utf8_spanish_ci NOT NULL,
		nombre varchar(500) COLLATE utf8_spanish_ci NOT NULL, direccion varchar(500) COLLATE utf8_spanish_ci NOT NULL, vigencia varchar(500) COLLATE utf8_spanish_ci NOT NULL,
		avaluo varchar(500) COLLATE utf8_spanish_ci NOT NULL, predial varchar(500) COLLATE utf8_spanish_ci NOT NULL, valor varchar(500) COLLATE utf8_spanish_ci NOT NULL,
		tipo int(11) NOT NULL DEFAULT '0', data_upload timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;";
		$rpta_create = mysqli_query($con,$stmt)or die(mysqli_error($con));
		$add_primary = "ALTER TABLE datos_".$nombre_fichero." ADD id BIGINT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY FIRST";
		$rpta_add_prk = mysqli_query($con,$add_primary)or die(mysqli_error($con));
		$add_index = "ALTER TABLE datos_".$nombre_fichero."	ADD KEY nombre (nombre(255));";
		$rpta_index = mysqli_query($con,$add_index);

		$sql = "LOAD DATA INFILE'".$carpeta.$n_archivo."' INTO TABLE datos_".$nombre_fichero." FIELDS TERMINATED BY ';' IGNORE 1 LINES";
		
		$rpta_archivo = mysqli_query($con,$sql)or die(mysqli_error($con));
		
		/* Set Juridicos */
		$update_jur = "UPDATE datos_".$nombre_fichero." set tipo = 1 WHERE nombre LIKE '%-SAS' or nombre LIKE '%LTDA%' or nombre LIKE '%S-A-S%' or nombre LIKE '%SOCIEDAD%' or nombre LIKE '%s.a%' or nombre LIKE '%asociacion%' or nombre LIKE '%compañia%' or nombre LIKE '%inversiones%' OR NOMBRE LIKE '%e hijos%' or nombre LIKE '%CIA S EN C%' or nombre LIKE '%INMOBILIARIA%' or nombre LIKE '%URBANIZACION%' or nombre like '%URBANIZACIÓN%' or nombre like '%IMSTITUTO%' or nombre like '%INSTITUTO%' or nombre like '%CORPORACION%' OR nombre like '%E-S-E%' or nombre like '%AEROCIVIL%' or nombre like '%ACUEDUCTO%' or nombre like '%AEROPUERTO%' or nombre like '%FUNDACION%' or nombre like '%PLAN%' or nombre like '%-S-A-S%' or nombre like '%COORPORACION%' or nombre like '%CORSOCIAL%' or nombre like '%AGENCIA%' or nombre like '%COMUNIDAD%'";

		$rpta_jur = mysqli_query($con, $update_jur);

		/* Set Religiosos */
		$update_relg = "UPDATE datos_".$nombre_fichero." SET tipo = 2 WHERE nombre LIKE '%IGLESIA%' OR nombre LIKE '%DIOSECIS%'
		OR nombre LIKE '%PARROQUIA%' OR nombre LIKE '%CENTRO CRISTIANO%' OR nombre LIKE '%CENTRO-CRISTIANO%' OR nombre LIKE '%ORDEN%' OR nombre LIKE '%IGLECIA%' OR nombre LIKE '%CONGREGACION%'";
		$rpta_relg = mysqli_query($con, $update_relg);

		/* Set Institucionales */
		$update_inst = "UPDATE datos_".$nombre_fichero." SET tipo = 3 WHERE nombre like '%JUNTA-ACCION-COMUNAL-ESCUELA%' OR nombre LIKE '%MINISTERIO%' or nombre like '%INSTITUTO%' OR nombre like '%COLEGIO%' OR nombre like '%POLICIA%'";
		$rpta_inst = mysqli_query($con,$update_inst);

		/* Set Municipales */
		$update_municipio = "UPDATE datos_".$nombre_fichero." SET tipo = 4 WHERE nombre like '%MUNICIPIO%' OR nombre LIKE '%PARQUE%'";
		$rpta_municipio= mysqli_query($con,$update_municipio);


		$obt_asociados = "SELECT nombre, COUNT(nombre) AS total FROM datos_".$nombre_fichero." WHERE ( direccion like 'C %' or direccion LIKE 'K %' or direccion LIKE '%Lote %' or direccion like '% MZ %')	AND tipo = 0 GROUP BY nombre having COUNT(nombre) > 10	ORDER BY COUNT(nombre) ASC";

		$result_asociados = mysqli_query($con,$obt_asociados);
		if (!$result_asociados){
			die("Error");
		}else{
			while ($nombre = mysqli_fetch_assoc($result_asociados)){
				/* Set Asociados */
				$sql = sprintf("UPDATE datos_".$nombre_fichero." SET tipo = 5 WHERE nombre = '%s' AND tipo = 0", $nombre['nombre']);
				$resp = mysqli_query($con, $sql);
			}
		}

		$obt_urbanos = "SELECT nombre, COUNT(nombre) AS total FROM datos_".$nombre_fichero." 
		WHERE ( direccion like 'C %' or direccion LIKE 'K %' or direccion LIKE '%Lote %' or direccion like '% MZ %')	AND tipo = 0 GROUP BY nombre having COUNT(nombre) <= 10	ORDER BY COUNT(nombre) ASC";

		$result_urbanos = mysqli_query($con,$obt_urbanos);
		if (!$result_urbanos){
			die("Error");
		}else{
			while ($nombre = mysqli_fetch_assoc($result_urbanos)){
				/* Set Urbanos */
				$sql = sprintf("UPDATE datos_".$nombre_fichero." SET tipo = 6 WHERE nombre = '%s' AND tipo = 0", $nombre['nombre']);
				$resp = mysqli_query($con, $sql);
			}
		}

		if (!$rpta_archivo) {
			echo '<script>
						swal.fire({
							title:"Opps!!",
							type:"error",
							html:"Hubo un problema al momento de importar el archivo. Por favor vuelva a intentarlo"
						});
					</script>';
			exit;
		}
		echo '<script>
						swal.fire({
							title:"Genial!!",
							type:"success",
							html:"Los datos han sido almacenados"
						});
						location.reload();
					</script>'
		;
		exit();
	}